<?php

namespace Libs;

defined('ROOT_DIR') or define('ROOT_DIR', __DIR__ . '/..');

class CitiesTest extends \PHPUnit_Framework_TestCase
{
    private $city;
    private $city_valid = [
        'iso_3166_3'=>'ABA',
        'country_code'=>'RU',
        'name'=>'Abakan'
    ];

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        // cities.json row "25":{"iso_3166_3":"ABA","country_code":"RU","name":"Abakan"},
        $this->city = new Cities();
    }


    /**
     * Проверяем существование файла
     */
    public function testCityFileExist(){
        $this-> assertFileExists(ROOT_DIR . '/resources/cities.json');
    }

    /**
     * Проверяем что данные в файле и в массиве с городами не пустые
     */
    public function testCityEmpty(){
        $this->assertStringNotEqualsFile(ROOT_DIR . '/resources/cities.json', '');
        $this->assertNotEmpty($this->city->getCities());
    }

    /**
     * Проверяем что массив данных соответствует нужному
     */
    public function testGetCityByCode_Equals(){
        $city_code = 'ABA';
        $result = $this->city->getCityByCode($city_code);
        $this->assertEquals($this->city_valid, $result);
    }

    /**
     * не верный код города возвращает false
     */
    public function testGetCityByCode_False(){
        $bad_city_code = 'ABAADSDSA';
        $result = $this->city->getCityByCode($bad_city_code);
        //Проверяем что не верный код города
        $this->assertFalse($result);
    }

    /**
     * Проверяем что массив данных соответствует нужному
     */
    public function testGetCityByName_Equals(){
        $city_name = 'Abakan';
        $result = $this->city->getCityByName($city_name);
        $this->assertEquals($this->city_valid, $result);
    }

    /**
     * Проверяем на неверное название города
     */
    public function testGetCityByName_False(){
        $bad_city_name = 'Так город точно не назовут';
        $result = $this->city->getCityByName($bad_city_name);
        $this->assertFalse($result);
    }

    /**
     * Проверяем что массив данных соответствует нужному
     */
    public function testGetOne_Equals(){
        $id = 25;
        $result = $this->city->getOne($id);
        $this->assertEquals($this->city_valid, $result);
    }

    /**
     * Проверяем что неверный id
     */
    public function testGetOne_False(){
        $bad_id = 123457457454098;
        $result = $this->city->getOne($bad_id);
        $this->assertFalse($result);
    }

    /**
     * Проверяем что вернулся массив
     */
    public function testGetList_isArray_True(){
        $result = $this->city->getList();
        $this->assertTrue(is_array($result));
    }

    /**
     * Проверяем что вернулся массив
     */
    public function testGetListForSelect_isArray_True(){
        $result = $this->city->getListForSelect();
        $this->assertTrue(is_array($result));
    }
}
