<?php

namespace Libs;

use Libs\Soap\MySoapClient;

defined('ROOT_DIR') or define('ROOT_DIR', __DIR__ . '/..');

class PingTest extends \PHPUnit_Framework_TestCase
{
    public function testMethod(){
        $mock = $this->getMockFromWsdl(MySoapClient::$wsdl_url);
        $mock->method('ETM_Ping')->willReturn(true);
    }

    public function testRequest(){
        $ping = new Ping();
        $ping_response = $ping->call();
        $this->assertFalse($ping_response['error']);
    }
}
