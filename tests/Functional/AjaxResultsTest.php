<?php

namespace Tests\Functional;

class AjaxResultsTest extends BaseTestCase
{
    public function testPost()
    {
        $request_id = 10;
        $response = $this->runApp('POST', "/ajax/results/{$request_id}");
        $this->assertEquals(201, $response->getStatusCode());
        $data = json_decode($response->getBody(true), true);

        $this->assertArrayHasKey('status', $data);
        $this->assertTrue(in_array($data['status'], ['success','error']));

        if($data['status']=='success'){
            $this->assertArrayHasKey('response', $data);
        }

        if($data['status']=='error'){
            $this->assertArrayHasKey('error', $data);
        }
    }

    public function testOther()
    {
        $request_id = 10;
        $method = ['GET','PUT', 'DELETE'];
        foreach ($method as $m){
            $response = $this->runApp($m, "/ajax/results/{$request_id}");
            $this->assertEquals(405, $response->getStatusCode(), $m);
        }
    }
}