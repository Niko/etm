<?php

namespace Tests\Functional;

class HomepageTest extends BaseTestCase
{

    public function testGet()
    {
        $response = $this->runApp('GET', '/');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('E-Tickets', (string)$response->getBody());
        $this->assertNotContains('Hello', (string)$response->getBody());
    }

    public function testPost()
    {
        $response = $this->runApp('POST', '/', [
            'departure_datetime' => date('Y-m-d'),
            'original_location' => 'Kaliningrad',
            'destination_location' => 'Moscow',
            'passangers_quantity' => 1,
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('E-Tickets', (string)$response->getBody());
    }

    public function testOther()
    {
        $method = ['PUT', 'DELETE'];
        foreach ($method as $m){
            $response = $this->runApp($m, '/');
            $this->assertEquals(405, $response->getStatusCode(), $m);
        }

    }
}