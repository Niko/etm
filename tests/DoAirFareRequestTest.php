<?php
namespace Libs;

use Libs\Soap\MySoapClient;

defined('ROOT_DIR') or define('ROOT_DIR', __DIR__ . '/..');

class DoAirFareRequestTest extends \PHPUnit_Framework_TestCase
{
    public function testMethod()
    {
        $mock = $this->getMockFromWsdl(MySoapClient::$wsdl_url);
        $mock->method('ETM_DoAirFareRequest')->willReturn(true);
    }

    public function testRequest()
    {
        $air_request = new DoAirFareRequest();
        $air_request->departure_datetime    = date('Y-m-d');
        $air_request->original_location     =  false;
        $air_request->destination_location  =  false;
        $air_request->passangers_quantity   = 1;

        $air_response = $air_request->call();
        $request_id = isset($air_response['response']->RequestId) ? $air_response['response']->RequestId : null;
        $this->assertNotEmpty($request_id);
    }
}
