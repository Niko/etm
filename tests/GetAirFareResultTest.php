<?php

namespace Libs;


use Libs\Soap\MySoapClient;

defined('ROOT_DIR') or define('ROOT_DIR', __DIR__ . '/..');

class GetAirFareResultTest extends \PHPUnit_Framework_TestCase
{
    public function testMethod(){
        $mock = $this->getMockFromWsdl(MySoapClient::$wsdl_url);
        $mock->method('ETM_GetAirFareResult')->willReturn(true);
    }
}
