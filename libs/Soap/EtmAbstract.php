<?php

namespace Libs\Soap;

use Libs\Soap\MySoapClient;

abstract class EtmAbstract
{
    private $soap_client;

    public function __construct()
    {
        $this->getSoapClient();
    }
    
    public function getSecurityData()
    {
        $security = new \stdClass();
        
        $security->Username = new \SoapVar('test1', XSD_STRING);
        $security->Password = new \SoapVar('7mkn6XC9L54p', XSD_STRING);
        $security->HashKey  = new \SoapVar('b2cbf0f711', XSD_STRING);
        
        $security_soap = new \SoapVar($security, SOAP_ENC_OBJECT, 'SecurityType', null, 'Security');
        
        return $security_soap;
    }
    
    public function getSoapClient()
    {
        try{
            if(!isset($this->soap_client)) {
                $this->soap_client = new MySoapClient("https://soap.etm-system.ru/etm-webservice-0.3.1/server.php?WSDL", [
                    "trace"          => true,
                    "exception"      => true,
                    'soap_version'   => SOAP_1_1,
                ]);
            }
            return $this->soap_client;
        }catch (\Exception $e){
            return null;
        }

    }

    public function call()
    {
        try
        {
            $method_name = $this->getMethodName();

            $request = $this->getRequest();

            $response = $this->getSoapClient()->$method_name($request);
            
            if (isset($response->Errors))
            {
                return [
                    'error'    => true,
                    'code'     => $response->Errors->Code ?? 0,
                    'response' => $response->Errors->Message ?? ''];
            }
            
            return ['error' => false, 'response' => $response];
            
        } catch (\SoapFault $fault) {
            //return ['error' => true, 'response' => $fault];
            return ['error' => true, 'response' => 'Bad request!'];
        }
    }
    
    abstract public function getRequest();
    abstract protected function getMethodName();
}
